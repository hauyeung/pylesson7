'''
Created on Jun 10, 2014

@author: hauyeung
'''
from tkinter import *

root = Tk()
root.wm_title("Converter")
var = StringVar()
def convert():
    entrytext = e.get()
    entrytext2 = e2.get()
    try:
        num = float(entrytext)
        num2 =float(entrytext2)
        var.set(num + num2)
        root.update_idletasks()
    except ValueError:
        var.set("***ERROR***")
        root.update_idletasks()
    
e = Entry(root)
e.pack()
e2 = Entry(root)
e2.pack()
b = Button(root, text='Convert', width=10, command=convert)
b.pack()
l = Label(root, textvariable= var)
l.pack()

root.mainloop()

